from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'solucion/index.html')

def indexAdmin(request):
    return render(request, 'solucion/indexAdmin.html')

def indexEstud(request):
    return render(request, 'solucion/indexEstud.html')

def indexProfe(request):
    return render(request, 'solucion/indexProfe.html')

def adGrupoEdit(request):
    return render(request, 'solucion/AdGrupoEdit.html')

def adGrupoCrear(request):
    return render(request, 'solucion/AdGrupoCrear.html')

def adGrupoVer(request):
    contexto = {
        'grupos': lista_grupos
    }
    return render(request, 'solucion/AdGrupoVer.html', contexto)

def adGrupoVer2(request, id):
    for grupo in lista_grupos:
        if grupo['id'] == id:
            contexto = {
                'caracter': grupo,
                'estudiante': lista_estudiantes 
            }
            return render(request,'solucion/AdGrupoVer2.html', contexto)
  
    return render(request, 'solucion/AdGrupoVer2.html')

def adGrupoAddEstudy(request):
    return render(request, 'solucion/AdGrupoAddEstudy.html')

def adProfeCrear(request):
    return render(request, 'solucion/AdProfeCrear.html')

def adProfeVer(request):
    contexto = {
    'profesores':lista_profesores
    }
    return render(request, 'solucion/AdProfeVer.html', contexto)

def adProfeVer2(request, id):
    for profesor in lista_profesores:
        if profesor['id'] == id:
            contexto = {
                'caracter': profesor,
                'grupo': lista_grupos
            }
            return render(request, 'solucion/AdProfeVer2.html', contexto)
    
    return render(request, 'solucion/AdProfeVer2.html')

def adEstudEdit(request):
    return render(request, 'solucion/AdEstudEdit.html')

def adEstudCrear(request):
    return render(request, 'solucion/AdEstudCrear.html')

def adEstudVer(request):
    contexto = {
        'estudiantes': lista_estudiantes
    }
    return render(request, 'solucion/AdEstudVer.html', contexto)

def proActivEdit(request):
    return render(request, 'solucion/ProActivEdit.html')

def proActivCrear(request):
    return render(request, 'solucion/ProActivCrear.html')

def proActivVer(request):
    contexto = {
        'grupo': lista_grupos[1],
        'estudiante': lista_estudiantes,
        'actividad': lista_actividades
    }
    
    return render(request, 'solucion/ProActivVer.html', contexto)

def proActivCalif(request):
    return render(request, 'solucion/ProActivCalif.html')

def proGrupoVer(request):
    contexto = {
        'grupo': lista_grupos
    }
    return render(request, 'solucion/ProGrupoVer.html', contexto)

def proGrupoVer2(request, id):
    for grupo in lista_grupos:
        if grupo['id'] == id:
            contexto={
                'caracter': grupo,
                'estudiantes': lista_estudiantes,
                'actividad': lista_actividades 
            }
            return render(request, 'solucion/ProGrupoVer2.html', contexto)
    
    return render(request, 'solucion/ProGrupoVer2.html')

def proEstudVer(request):
    contexto = {
        'estudiante': lista_estudiantes[0],
        'actividad': lista_actividades
    }
    
    return render(request, 'solucion/ProEstudVer.html', contexto)

def estudianteVer(request):
    contexto = {
        'grupos': lista_grupos
    }
    
    return render(request, 'solucion/EstudianteVer.html', contexto)

def estudGrupoVer(request):
    contexto = {
        'grupo': lista_grupos[1],
        'actividad': lista_actividades
    }
    
    return render(request, 'solucion/EstudGrupoVer.html', contexto)

#Data Base

lista_grupos = [
    {'id': 1, 'cod': 123456, 'asig': 'Calculo diferencial', 'profe': 'Fredy Cortez', 'num_estud': 20, 'sem': 'Primero'},
    {'id': 2, 'cod': 459678, 'asig': 'Calculo Integral', 'profe': 'Carlos Acevedo', 'num_estud': 15, 'sem': 'Segundo'},
    {'id': 3, 'cod': 789456, 'asig': 'Filosofia Política', 'profe': 'Fernando Chavez', 'num_estud': 10, 'sem': 'Tercero'},
]

lista_estudiantes = [
    {'id': 4, 'cod': 147258, 'name': 'Andrés Felipe', 'apel': 'Castro Perilla', 'email': 'felipecastro@usantotomas.edu.co'},
    {'id': 5, 'cod': 369258, 'name': 'Olmer Steven', 'apel': 'Suarez Giraldo', 'email': 'olmersuarez@usantotomas.edu.co'},
    {'id': 6, 'cod': 852963, 'name': 'Carlos Arturo', 'apel': 'Torres Castaño', 'email': 'carlostorres@usantotomas.edu.co'},
]

lista_profesores = [
    {'id': 7, 'name': 'Angela Tatiana', 'apel': 'Zona Ortiz', 'email': 'tatianazona@usantotomas.edu.co'},
    {'id': 8, 'name': 'Gerald Breek', 'apel': 'Fuenmayor Rivadeneira', 'email': 'geraldfuenmayor@usantotomas.edu.co'},
    {'id': 9, 'name': 'Fernando', 'apel': 'Prieto Bustamante', 'email': 'fernandoprieto@usantotomas.edu.co'},
]

lista_actividades = [
    {'id': 10, 'name': 'Actividad 1', 'act':'Aqui va la actividad numero 1', 'nota':4.2,},
    {'id': 11, 'name': 'Actividad 2', 'act':'Aqui va la actividad numero 2', 'nota':4.5,},
    {'id': 12, 'name': 'Actividad 3', 'act':'Aqui va la actividad numero 3', 'nota':4.3,},
]