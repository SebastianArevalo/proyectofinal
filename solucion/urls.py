from django.urls import path 
from . import views 

app_name = 'solucion' 
urlpatterns = [
    path('', views.index, name='index'),
    path('indexAdmin/', views.indexAdmin, name='indexAdmin'),
    path('indexEstud/', views.indexEstud, name='indexEstud'),
    path('indexProfe/', views.indexProfe, name='indexProfe'),
    path('adGrupoEdit/', views.adGrupoEdit, name='adGrupoEdit'),
    path('adGrupoCrear/', views.adGrupoCrear, name='adGrupoCrear'),
    path('adGrupoVer/', views.adGrupoVer, name='adGrupoVer'),
    path('adGrupoVer2/<int:id>/', views.adGrupoVer2, name='adGrupoVer2'),
    path('adGrupoAddEstudy/', views.adGrupoAddEstudy, name='adGrupoAddEstudy'),
    path('adProfeCrear/', views.adProfeCrear, name='adProfeCrear'),
    path('adProfeVer/', views.adProfeVer, name='adProfeVer'),
    path('adProfeVer2/<int:id>/', views.adProfeVer2, name='adProfeVer2'),
    path('adEstudEdit/', views.adEstudEdit, name='adEstudEdit'),
    path('adEstudCrear/', views.adEstudCrear, name='adEstudCrear'),
    path('adEstudVer/', views.adEstudVer, name='adEstudVer'),
    path('proActivEdit/', views.proActivEdit, name='proActivEdit'),
    path('proActivCrear/', views.proActivCrear, name='proActivCrear'),
    path('proActivVer/', views.proActivVer, name='proActivVer'),
    path('proActivCalif/', views.proActivCalif, name='proActivCalif'),
    path('proGrupoVer/', views.proGrupoVer, name='proGrupoVer'),
    path('proGrupoVer2/<int:id>/', views.proGrupoVer2, name='proGrupoVer2'),
    path('proEstudVer/', views.proEstudVer, name='proEstudVer'),
    path('estudianteVer/', views.estudianteVer, name='estudianteVer'),
    path('estudGrupoVer/', views.estudGrupoVer, name='estudGrupoVer')
]